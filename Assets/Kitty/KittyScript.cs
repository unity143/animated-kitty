using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class KittyScript : MonoBehaviour
{
    
    [SerializeField] float speed = 10;
    [SerializeField] float speed_run = 20;
    [SerializeField] float speed_jump = 10;  

    bool jump;    
    float direction;
    float run;
    Rigidbody2D kitty_body;
    Collider2D kitty_collider;
    SpriteRenderer kitty_sprite_renderer;
    Animator kitty_animator;

    // Start is called before the first frame update
    void Start()
    {
        jump = false;
        run = 0;
        direction = 0;
        kitty_body = GetComponent<Rigidbody2D>();
        kitty_collider = GetComponent<Collider2D>();
        kitty_sprite_renderer = GetComponent<SpriteRenderer>();
        kitty_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {        
        Vector2 velocity = kitty_body.velocity;         

        // Jumping
        if (jump && kitty_collider.IsTouchingLayers(LayerMask.GetMask("Tiles")))
        {
            velocity.y = speed_jump;
            // Reset Jump stats + check for failing
            jump = false;
            kitty_animator.SetBool("isJumping", false);            
        }

        kitty_animator.SetBool("isFalling", !kitty_collider.IsTouchingLayers(LayerMask.GetMask("Tiles")));

        // Running or Walking
        if (run > 0)
        {            
            velocity.x += direction * speed_run * Time.fixedDeltaTime;           
        }
        else {
            velocity.x += direction * speed * Time.fixedDeltaTime;
            
        } 
        kitty_body.velocity = velocity;      

    }

    void OnJump() 
    {
        jump = true;
        kitty_animator.SetBool("isJumping", true);       
    }

    void OnRun(InputValue input)
    {
        run = input.Get<float>();
        kitty_animator.SetFloat("isRunning", Math.Abs(run)); 
    }

    private void OnMove(InputValue input)
    {
        direction = input.Get<float>();
        if (direction < 0)
        {
            kitty_sprite_renderer.flipX = true;
            
        }
        if (direction > 0)
        {
            kitty_sprite_renderer.flipX = false;            
        }
        kitty_animator.SetFloat("isWalking", Math.Abs(direction));
    }
}
